<?php
//chama o arquivo de conexão com o bd
include ("db.php");

$queryConta = "select count(*) from contacto ";
$res = &$mdb2 -> query($queryConta);
if (PEAR::isError($res)) {
	die($res -> getMessage());
}
$row = $res -> fetchRow(MDB2_FETCHMODE_ASSOC);
$total = $row['count'];

$sql  = "select id, nome, telefone, email from contacto ";
$res = &$mdb2 -> query($sql);
if (PEAR::isError($res)) {
	die($res -> getMessage());
}

$tabela = array();
while ($row = $res -> fetchRow(MDB2_FETCHMODE_ASSOC)) {
	array_push($tabela, $row);
}
$resposta["contactos"] = $tabela;
$resposta["success"] = true;
$resposta["total"] = $total;

header('Content-type: application/json');
echo json_encode($resposta);
$mdb2 -> disconnect();
?>